import os
import time
import logging

import motor
import pymongo
import tornado.web
import tornado.escape
import tornado.auth
import tornado.gen
import tornado.ioloop
import tornado.httpserver

from bson.objectid import ObjectId
from tornado.options import define, options, parse_command_line

from handlers import BaseHandler


define("port", default=9877, help="run on the given port", type=int)
define("database", default='test', help="run on the database")

DEBUG_MODE = True
BASE_DIR = os.path.dirname(__file__)

mongo_client = motor.MotorClient('localhost', 27017)


class IndexHandler(BaseHandler):

    def get(self):

        user = self.get_current_user()
        self.set_cookie('_xsrf', self.xsrf_token)

        data = {
            'user': user
        }

        self.render("index.html", **data)


class GamesHandler(BaseHandler):

    @tornado.web.asynchronous
    @tornado.gen.coroutine
    def get(self, game_id=None):
        response = []

        if not game_id:
            cursor = self.db.games.find({'date': {'$gte': int(time.time())}}).sort('date', pymongo.ASCENDING)

            while (yield cursor.fetch_next):
                game = cursor.next_object()
                game['id'] = str(game['_id'])
                del game['_id']
                response.append(game)
        else:
            game = yield self.db.games.find_one({'_id': ObjectId(game_id)}, {'_id': 0})
            response.append(game)

        self.set_status(200)
        self.set_header('Content-Type', 'application/json')

        self.write(tornado.escape.json_encode(response))
        self.finish()

    @tornado.gen.coroutine
    def post(self, *args, **kwargs):

        data = self.request.body
        data = tornado.escape.json_decode(data)

        if not len(data.get('address', '')):
            raise tornado.web.HTTPError(400)

        res = yield tornado.gen.Task(self.db.games.insert, data)

        return self.finish()



class Application(tornado.web.Application):

    def __init__(self):

        handlers = [
            ('/?', IndexHandler),
            ('/games/$', GamesHandler),
            ('/games/(?P<game_id>\w+\d+)/', GamesHandler),
        ]

        settings = dict(
            cookie_secret="7db88qptLoldi0o3ty121WY3coUUdH2qq10btw0shddly6h826bAADbp",
            xsrf_cookies=False,
            template_path=os.path.join(BASE_DIR, 'templates'),
            static_path=os.path.join(BASE_DIR, 'static'),
            facebook_api_key = 374711522588867,
            facebook_secret = '6cda33e60d605eb3d27b04658e9b2529',
            autoescape = None,
            debug=DEBUG_MODE,
        )

        self.db = mongo_client.mafia

        tornado.web.Application.__init__(self, handlers, **settings)

if __name__ == '__main__':

    address = '127.0.0.1'

    logging.info('*********** Start server **********')
    logging.debug('DEBUG_MODE=%s' % DEBUG_MODE)
    parse_command_line()
    app = Application()

    print('Server started http://{0}:{1}...'.format(address, options.port))

    app.listen(options.port, address=address)
    tornado.ioloop.IOLoop.instance().start()

