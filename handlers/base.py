
import tornado.web
import tornado.gen


class BaseHandler(tornado.web.RequestHandler):

    @property
    def db(self):
        return self.application.db

    def get_current_user(self):
        user = str(self.get_secure_cookie('user'))
        return user